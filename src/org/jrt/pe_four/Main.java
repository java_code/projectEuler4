/**
 * org.jrt.pe_four is a list of operations to complete project euler problem four
 */

package org.jrt.pe_four;

/**
 * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 � 99.
 * 
 * Find the largest palindrome made from the product of two 3-digit numbers.
 *  
 * @author jrtobac
 *
 */
public class Main {

	public static void main(String[] args) {
		
		System.out.println(findMax());
		
	}
	
	/**
	 * Find the maximum palindrome
	 * 
	 * @return maximumPalindrome that can be found from two three digit numbers
	 */
	static int findMax() {
		int prod = 0;
		int maxPalindrome = 0;
		
		for(int numA = 999; numA > 99; numA--) {
			for(int numB = 999; numB >= numA; numB--) {
				prod = numA * numB;
				if(prod < maxPalindrome) {
					break;
				}
				if(palindrome(prod)) {
					maxPalindrome = prod;
				}
			}
		}
		return maxPalindrome;
	}
	
	/**
	 *  Check if a number is a palindrome
	 * @param numA
	 * @return true if the number is a palindrome
	 * @return false if the number is not a palindrome
	 */
	static boolean palindrome(int numA) {
		int numB = reverseDigits(numA);
		
		if(numA == numB) {
			return true;
		}
		return false;
	}
	
	/**
	 * reverse a number
	 * @param num
	 * @return revNum the reverse of the input number
	 */
	static int reverseDigits(int num)
	{
	    int revNum = 0;
	    while (num > 0)
	    {
	        revNum = revNum*10 + num%10;
	        num = num/10;
	    }
	    return revNum;
	}

}
